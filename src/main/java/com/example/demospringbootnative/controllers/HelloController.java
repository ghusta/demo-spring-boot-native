package com.example.demospringbootnative.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class HelloController {

    @GetMapping(path = "/hello")
    public ResponseEntity<Object> hello() {
        Map<String, Object> result = Map.of("message", "Hello !");
        return ResponseEntity.ok(result);
    }

}
