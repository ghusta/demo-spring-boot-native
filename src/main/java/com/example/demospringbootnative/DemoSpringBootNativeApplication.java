package com.example.demospringbootnative;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringBootNativeApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringBootNativeApplication.class, args);
	}

}
